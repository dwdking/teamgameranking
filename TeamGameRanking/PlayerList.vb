﻿Imports System.IO

Public Class PlayerList

	Private _players As List(Of Player)

	Public Sub New()
		_players = New List(Of Player)()

		Dim playerReadLine As String
		Dim pointReadLine As String
		Dim splitLine() As String

		Using playerReader As New StreamReader("Players.txt")
			Using pointReader As New StreamReader("PlayerAveragePoints.txt")
				While Not playerReader.EndOfStream
					playerReadLine = playerReader.ReadLine()
					pointReadLine = pointReader.ReadLine()

					splitLine = playerReadLine.Split(","c)

					_players.Add(New Player(splitLine(0), TeamList.GetTeamByAcronym(splitLine(1).Trim().Split(" "c)(0).Trim().ToUpper()), CDbl(pointReadLine)))
				End While
			End Using
		End Using
	End Sub

	Public Function EstimatedWeeklyPoints() As Double
		Dim totalPoints As Double = 0

		For Each value As Player In _players
			totalPoints += value.EstimatedPointsForWeek
		Next

		Return totalPoints
	End Function

End Class