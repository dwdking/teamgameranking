﻿Public Class Team

	Private Shared _teamLength As Integer

	Public Shared Property TeamLength As Integer
		Get
			Return _teamLength
		End Get
		Set(value As Integer)
			_teamLength = value
		End Set
	End Property

	Private _name As String
	Private _acronym As String
	Private _games As Integer

	Public Property Name As String
		Get
			Return _name
		End Get
		Set(value As String)
			_name = value
		End Set
	End Property

	Public Property Acronym As String
		Get
			Return _acronym
		End Get
		Set(value As String)
			_acronym = value
		End Set
	End Property

	Public Property Games As Integer
		Get
			Return _games
		End Get
		Set(value As Integer)
			_games = value
		End Set
	End Property

	Public Sub New(name As String, acronym As String, games As Integer)
		_name = name
		_acronym = acronym
		_games = games
	End Sub

	Public Overrides Function Equals(obj As Object) As Boolean
		Dim teamObj As Team = TryCast(obj, Team)

		If teamObj Is Nothing Then
			Return False
		End If

		If teamObj.Name = _name Then
			Return True
		Else
			Return False
		End If
	End Function

	Public Overrides Function ToString() As String
		Return _name.PadRight(Team.TeamLength, " "c) & vbTab & _games
	End Function

End Class