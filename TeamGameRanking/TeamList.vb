﻿Imports System.IO

Public Class TeamList

	Private Shared _list As TeamList = Nothing

	Public Shared Function GetTeamList() As TeamList
		If _list Is Nothing Then
			_list = New TeamList()
		End If

		Return _list
	End Function

	Public Shared Function GetTeamByAcronym(acronym As String) As Team
		GetTeamList()

		Return _list._teams.FirstOrDefault(Function(c) c.Acronym = acronym)
	End Function

	Private _teams As List(Of Team)

	Private Sub New()
		_teams = New List(Of Team)(30)

		Dim readLine As String = ""
		Dim splitLine() As String
		Dim readTeam As Team

		Using reader As New StreamReader("Teams.txt", Text.Encoding.Default)
			While Not reader.EndOfStream
				readLine = reader.ReadLine()
				splitLine = readLine.Split(","c)

				readTeam = New Team(splitLine(0).ToUpper().Trim(), splitLine(1).ToUpper().Trim(), 0)

				If readTeam.Name.Length > Team.TeamLength Then
					Team.TeamLength = readTeam.Name.Length
				End If

				_teams.Add(readTeam)
			End While
		End Using

		Using reader As New StreamReader("TeamGames.txt", Text.Encoding.Default)
			While Not reader.EndOfStream
				readLine = reader.ReadLine().ToUpper().Trim()

				AddGame(readLine)
			End While
		End Using

		_teams.Sort(AddressOf CompareGamesPlayed)
	End Sub

	Private Function AddGame(teamName As String) As Boolean
		Dim increaseTeam As Team = _teams.FirstOrDefault(Function(c) c.Name = teamName)

		If increaseTeam IsNot Nothing Then
			increaseTeam.Games += 1

			Return True
		Else
			Return False
		End If
	End Function

	Private Function CompareGamesPlayed(x As Team, y As Team) As Integer
		If x.Games > y.Games Then
			Return -1
		End If

		If x.Games < y.Games Then
			Return 1
		End If

		Return 0
	End Function

	Public Overrides Function ToString() As String
		Dim teamsString As New Text.StringBuilder()
		Dim teamCount As Integer = 1

		For Each value As Team In _teams
			teamsString.Append(teamCount.ToString().PadLeft(2))
			teamsString.Append(". ")
			teamsString.Append(value.ToString())
			teamsString.AppendLine()

			teamCount += 1
		Next

		Return teamsString.ToString().TrimEnd().TrimEnd("."c)
	End Function

End Class