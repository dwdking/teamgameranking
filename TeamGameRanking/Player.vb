﻿Public Class Player

	Private _name As String
	Private _team As Team
	Private _averagePoints As Double

	Public Property Name As String
		Get
			Return _name
		End Get
		Set(value As String)
			_name = value
		End Set
	End Property

	Public Property Team As Team
		Get
			Return _team
		End Get
		Set(value As Team)
			_team = value
		End Set
	End Property

	Public Property AveragePoints As Double
		Get
			Return _averagePoints
		End Get
		Set(value As Double)
			_averagePoints = value
		End Set
	End Property

	Public ReadOnly Property EstimatedPointsForWeek As Double
		Get
			Return _averagePoints * _team.Games
		End Get
	End Property

	Public Sub New(name As String, team As Team, averagePoints As Double)
		_name = name
		_team = team
		_averagePoints = averagePoints
	End Sub

End Class